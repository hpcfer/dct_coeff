Workshop instructions

dct_coeff

Clone the project from the public git repository: 
git clone https://gitlab.com/hpcfer/dct_coeff.git 

Compile the program
cd dct_coeff/build/linux
make

Run the program
./dct_coeff 

The output is jpeg file „dct_coeff.jpg” located in „dct_coeff/data”


Intructions	

Clone the project from the public git repository: 
git clone https://gitlab.com/hpcfer/workshop.git

Compile the program
cd workshop/build/linux
make

Run the program
./workshop

The output is benchmark comparing several kernels
Scalar implementation (pure C/C++) vs AVX intrinsics implementation

Notes
5 kernels are implemented and benchmarked
Sum of absoulute differences (SAD), multiply, DCT, quantization, and substract
All kernels are working with 8-bit or 16-bit input matrices
Default benchmark runs the kernel for 1.000.000 iterations for 32x32 blocks
„volatile” is used as an input to avoid unwanted compiler optimizations, since we are running the iterations with same inputs
You can use methods in Helpers class to print or compare to matrices

Intrinsics guide
For the demo we used some basic AVX instructions

https://www.intel.com/content/www/us/en/docs/intrinsics-guide/index.html
https://db.in.tum.de/~finis/x86-intrin-cheatsheet-v2.1.pdf


