// Implementation of full Baseline JPEG compression algorithm 
// Implementation for grayscale/luminance images ONLY 
// Dimension MUST be a multiple of 8 
// Floating point implementation of DCT, QUANT,... 
// (c) Mario Kovac, 2008 


#include <stdio.h>
#include <math.h>
#define PI 3.14159
#define Q_PRECISION 20
#define TRUE -1
long MASK=0xfffffff0;
int NUM_OF_TESTS=1;

void dct();
void getblock();
void entropy();
void order();

int in[8][8];
//int DBG_PRINT=TRUE;
int DBG_PRINT=0;
FILE *fq,*fpout;
int qtbl[8][8];
long q2tbl[8][8];
FILE *fp_dc,*fp_ac, *fp_test;
int dc_tbl[12][2],ac_tbl[16][11][2];
int bit_pos=8, nbytes=0;
char out_ch;
int X,Y,tot,tmptot;
long fileoffset;
int mydct[8][8];
char ch;

void main(argc, argv)
 int argc;
 char **argv;
 {


int u,v,i,j;

/* Reading external dct table */

	if ((fq=fopen("../../data/dct_tbl.txt","r"))==NULL) {
		printf ("dct_tbl.txt file open error! \n");
		exit(0);
	}
	for (v=0;v<8;v++) {
		for (u=0;u<8;u++) {
				 fscanf(fq,"%d",&mydct[v][u]);
		}
	}
	fclose(fq);

/* Reading external Huffman tables ( */

	if ((fp_dc=fopen("../../data/h_dc_lum.ttt","rb"))==NULL) {
		printf ("h_dc_lum.ttt file open error! \n");
		exit(0);
	}
	for (i=0;i<12;i++) {
		fread(&dc_tbl[i][0],2,1,fp_dc);	/// code length in bits
		fread(&dc_tbl[i][1],2,1,fp_dc); /// code word
	}
	fclose(fp_dc);

	if ((fp_ac=fopen("../../data/h_ac_lum_new.ttt","rb"))==NULL) {
		printf ("h_ac_lum_new.ttt file open error! \n");
		exit(0);
	}
	for (i=0;i<16;i++)								/// i= Run
		for (j=0;j<11;j++) {						/// j= Size
			if ((i==0) || (j>0) || (i==15)) {
				fread(&ac_tbl[i][j][0],2,1,fp_ac);	/// code length
				fread(&ac_tbl[i][j][1],2,1,fp_ac);	/// codeword
			}
		}
	fclose(fp_ac);

	if ((fp_test=fopen("../../data/header.jpg","rb"))==NULL) {
		printf("Cannot open header.jpg file\n");
		exit(0);
	}
	fpout=fopen("../../data/dct_coeff.jpg","wb");

	for (i=0;i<164;i++) {
		fread(&v,2,1,fp_test);
		fwrite(&v,2,1,fpout);
	}

	fclose(fp_test);

		entropy(mydct);

	/// last byte stuffing !!!

	if (bit_pos<8) {
		order (&out_ch,&bit_pos,bit_pos,0,&nbytes);
		/// Ovo bi valjda trebalo biti :
///				order (&out_ch,&bit_pos,8-bit_pos,0xFF,&nbytes);


	}
	u=0xd9ff;
	fwrite(&u,2,1,fpout);

	fclose(fpout);
	exit(0);
}


