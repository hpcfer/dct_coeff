#include <stdio.h>
struct rlstruct {
    int data, count;
    int dc,ac,zrl,eob;
};
struct pestruct {
    int data,code_size,count;
    int dc,ac,zrl,eob;
};
struct ststruct {
    int data,code_size,count;
    int dc,ac,zrl,eob;
	int flag;
};
struct codestruct {
    int data, dsize, huff, length;
    int ac,dc;
};
extern int dc_tbl[12][2],ac_tbl[16][11][2];
extern int bit_pos, nbytes;
extern char out_ch;
extern int DBG_PRINT;
int prev_dc=0;
extern FILE  *fpout;
struct ststruct stat[4]={{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0},{0,0,0,0,0,0,0}};

struct codestruct code;

void entropy();
void zigzag();
void runlen();
void pipe();
void status();
void codeinsert();
void pack();
void order();

char output[1000];


void entropy(block)
int *block;
{
int tmp,i;

struct rlstruct rl[64];
struct pestruct pe[64];
//struct codestruct code[64];


	zigzag(block);
	runlen(block,rl);
	pipe(rl,pe);
	for (i=0;i<64;i++) {
		status(&pe[i],&tmp,0);
		codeinsert();
		}
	for (i=0;i<3;i++) {
		status(&pe[0],&tmp,1);
		codeinsert();
		}
}

void zigzag(ib)
int *ib;
{
int t[64],x;
for (x=0;x<64;x++) t[x]=ib[x];
ib[0]=t[0];
ib[1]=t[1];
ib[2]=t[8];
ib[3]=t[16];
ib[4]=t[9];
ib[5]=t[2];
ib[6]=t[3];
ib[7]=t[10];
ib[8]=t[17];
ib[9]=t[24];
ib[10]=t[32];
ib[11]=t[25];
ib[12]=t[18];
ib[13]=t[11];
ib[14]=t[4];
ib[15]=t[5];
ib[16]=t[12];
ib[17]=t[19];
ib[18]=t[26];
ib[19]=t[33];
ib[20]=t[40];
ib[21]=t[48];
ib[22]=t[41];
ib[23]=t[34];
ib[24]=t[27];
ib[25]=t[20];
ib[26]=t[13];
ib[27]=t[6];
ib[28]=t[7];
ib[29]=t[14];
ib[30]=t[21];
ib[31]=t[28];
ib[32]=t[35];
ib[33]=t[42];
ib[34]=t[49];
ib[35]=t[56];
ib[36]=t[57];
ib[37]=t[50];
ib[38]=t[43];
ib[39]=t[36];
ib[40]=t[29];
ib[41]=t[22];
ib[42]=t[15];
ib[43]=t[23];
ib[44]=t[30];
ib[45]=t[37];
ib[46]=t[44];
ib[47]=t[51];
ib[48]=t[58];
ib[49]=t[59];
ib[50]=t[52];
ib[51]=t[45];
ib[52]=t[38];
ib[53]=t[31];
ib[54]=t[39];
ib[55]=t[46];
ib[56]=t[53];
ib[57]=t[60];
ib[58]=t[61];
ib[59]=t[54];
ib[60]=t[47];
ib[61]=t[55];
ib[62]=t[62];
ib[63]=t[63];
}

void runlen(block,rl)
int *block;
struct rlstruct *rl;
{
int i,cnt;


rl[0].dc=1;
rl[0].ac=0;
rl[0].zrl=0;
rl[0].eob=0;
rl[0].data=block[0]-prev_dc;
prev_dc=block[0];
if (rl[0].data<0) rl[0].data--;
rl[0].count=0;
cnt=0;

	for (i=1;i<64;i++) {
		rl[i].dc=0;
		rl[i].ac=(block[i])?1:0;
		rl[i].eob=((i==63)&&(!block[i]))?1:0;
		rl[i].zrl=((cnt==15)&&(!block[i])&&(i<63))?1:0;
		rl[i].count=(rl[i].eob)? 0:cnt;
		rl[i].data=block[i];
		if (block[i]&1024) block[i]|=2048;
		if (!block[i]) {
			cnt++;
			if (cnt==16) cnt=0;
		}
		if (rl[i].data!=0) cnt=0;
		if (rl[i].data<0) rl[i].data--;
	}
}

void pipe(rl,pe)
struct rlstruct *rl;
struct pestruct *pe;
{
int i,j,eq;

	for(j=0;j<64;j++) {
		eq=1;
		pe[j].count=rl[j].count;
		pe[j].data=rl[j].data;
		pe[j].code_size=11;
		pe[j].dc=rl[j].dc;
		pe[j].ac=rl[j].ac;
		pe[j].zrl=rl[j].zrl;
		pe[j].eob=rl[j].eob;
		for (i=0;i<11;i++){
			if (((rl[j].data&2048))==(((rl[j].data<<(i+1)))&2048)) eq*=1;
			else eq=0;
			if (eq) {
				pe[j].code_size=10-i;
			}
		}
	}
}

void status(pe,load,flush)
struct pestruct *pe;
int *load, flush;
 {
int i;

		*load=pe->dc|pe->ac|pe->zrl|pe->eob;
		if ((*load)||(flush)) {
			for (i=3;i>0;i--) {
				stat[i].data=stat[i-1].data;
				stat[i].code_size=stat[i-1].code_size;
				stat[i].count=stat[i-1].count;
				stat[i].dc=stat[i-1].dc;
				stat[i].ac=stat[i-1].ac;
				stat[i].zrl=stat[i-1].zrl;
				stat[i].eob=stat[i-1].eob;
				stat[i].flag=stat[i-1].flag;
			}
			stat[0].data=pe->data;
			stat[0].code_size=pe->code_size;
			stat[0].count=pe->count;
			stat[0].dc=(flush)?0:pe->dc;
			stat[0].ac=(flush)?0:pe->ac;
			stat[0].zrl=(flush)?0:pe->zrl;
			stat[0].eob=(flush)?0:pe->eob;
			stat[0].flag=(flush)?0:1;
			stat[3].zrl=stat[3].zrl*(1-(stat[2].eob|(stat[2].zrl*(stat[1].eob|(stat[1].zrl*stat[0].eob)))));
		}
}
void	codeinsert() {
	int hcode,hcodes;
		code.ac=stat[3].eob|stat[3].zrl|stat[3].ac;
		code.dc=stat[3].dc;
		if (stat[3].flag && (code.ac||code.dc)) {
		if (code.dc) {
			hcodes=dc_tbl[stat[3].code_size][0];
			hcode=dc_tbl[stat[3].code_size][1];
			if (DBG_PRINT) printf("  DC>%d<",stat[3].code_size);
		}
		else {
			hcodes=ac_tbl[stat[3].count][stat[3].code_size][0];
			hcode=ac_tbl[stat[3].count][stat[3].code_size][1];
			if (DBG_PRINT) printf("  AC>%d:%d<",stat[3].count,stat[3].code_size);
		}
		stat[3].flag=0;
		if (DBG_PRINT) printf("[%x(%d):%x(%d)]",hcode,hcodes,stat[3].data,stat[3].code_size);
		order (&out_ch,&bit_pos,hcodes,hcode,&nbytes);
		order (&out_ch,&bit_pos,stat[3].code_size,stat[3].data,&nbytes);
	}
}

void order(ch_pos,bit_pos,bit_count,data,nbytes)
char *ch_pos;
int *bit_pos,bit_count,data,*nbytes;
 {

	int cnt,i,mask;

	if (*bit_pos==8) *ch_pos=0;
	while (bit_count>0) {
		cnt=((*bit_pos-bit_count)>=0)? bit_count:*bit_pos;
		for (i=0,mask=0;i<cnt;i++,mask=mask*2+1) ;
		(*ch_pos) |= ((data>>(bit_count-cnt))&mask)<<(*bit_pos-cnt);
		bit_count -= cnt;
		(*bit_pos) -= cnt;
		if (*bit_pos==0) {
			*bit_pos=8;
			(*ch_pos)&=255;
			fwrite(ch_pos,1,1,fpout);
			if (((*ch_pos)&255)==255) {
				(*ch_pos)=0;
				fwrite(ch_pos,1,1,fpout);
			}
			(*ch_pos)=0;
			(*nbytes)++;
		}
	}
}
